import HTTP from '../HTTP';

const BASE = 'auth';

export async function login(payload) {
  const res = await HTTP.post(`${BASE}/database-login`, payload);
  return res;
}
export async function logout(payload) {
  const res = await HTTP.post(`${BASE}/logout`, payload);
  return res;
}

export async function googleLogin(payload) {
  const res = await HTTP.post(`${BASE}/google-login`, payload);
  return res;
}

export async function googleRegister(payload) {
  const res = await HTTP.post(`${BASE}/google-register`, payload);
  return res;
}

export async function register(payload) {
  const res = await HTTP.post(`${BASE}/database-register`, payload);
  return res;
}
