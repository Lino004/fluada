import configDev from './config.dev';
import configCI from './config.ci';
import configStaging from './config.staging';
import configProd from './config.prod';

// eslint-disable-next-line import/no-mutable-exports
let config;
if (process.env.NODE_ENV === 'production') {
  if (process.env.VUE_APP_BACK === 'staging') {
    config = configStaging;
  } else if (process.env.VUE_APP_BACK === 'ci') {
    config = configCI;
  } else {
    config = configProd;
  }
} else {
  config = configDev;
}

export default config;
