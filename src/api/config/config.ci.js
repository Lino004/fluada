const urlBase = '#{BASE_URL}#/';

export default {
  URL_SERVEUR: urlBase,
  BASE_URL: urlBase + 'v1/',
};
