const urlBase = 'https://domaine.com/';

export default {
  URL_SERVEUR: urlBase,
  BASE_URL: urlBase + 'v1/',
};
