const urlBase = 'http://localhost:8000/';

export default {
  URL_SERVEUR: urlBase,
  BASE_URL: urlBase + 'v1/',
};
