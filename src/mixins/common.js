export default {
  methods: {
    fdSnackbarError(text, title) {
      this.$snackbar.add({
        type: 'error',
        title: title || this.$t('snackbar.error.default'),
        text: text,
      });
    },
    fdActionErrors(error) {
      if (error?.response?.data) {
        if (error?.response?.data.error) {
          this.fdSnackbarError(
            error?.response?.data.error,
            this.$t('snackbar.error.errorServer'),
          );
        }
        if (error?.response?.data.errors) {
          error?.response?.data.errors.forEach(e => {
            this.fdSnackbarError(
              e.msg,
              this.$t('snackbar.error.errorServer'),
            );
          });
        }
      } else if (error?.message) {
        this.fdSnackbarError(
          error?.message,
          this.$t('snackbar.error.errorServer'),
        );
      }
    },
  },
};
