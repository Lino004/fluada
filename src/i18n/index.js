import { createI18n } from 'vue-i18n/index';
import messages from './translator';

const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'en',
  messages,
});

export default i18n;
