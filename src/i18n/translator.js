import fr from './fr';
import en from './en';

const messages = {
  fr: [],
  en: [],
};

function translate(val) {
  Object.keys(val).forEach((name) => {
    if (typeof val[name] === 'string') {
      val[name] = '' + val[name];
    } else {
      translate(val[name]);
    }
  });
}

function allTraduction(allTraduction, generalTraduction) {
  Object.keys(allTraduction).forEach((key) => {
    generalTraduction[key] = allTraduction[key];
  });
}

allTraduction(fr, messages.fr);
allTraduction(en, messages.en);

if (process.env.NODE_ENV === 'development') {
  Object.keys(messages).forEach(key => {
    translate(messages[key]);
  });
}

export default messages;
