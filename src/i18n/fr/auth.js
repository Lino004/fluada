export default {
  logout: 'Déconnexion',
  signin: {
    title: 'Connexion',
    subtitle: 'Vous êtes nouveau ici ?',
    linkActionSignUp: 'Inscrivez-vous ici',
    form: {
      email: 'Email',
      password: 'Mot de passe',
      continue: 'Continuer',
      authGoogle: 'Continuer avec Google',
    },
  },
  signup: {
    title: 'Inscription',
    subtitle: 'Vous avez déjà un compte ?',
    linkActionSignIn: 'Connectez-vous ici',
    form: {
      fistName: 'Prénom(s)',
      lastName: 'Nom',
      email: 'Email',
      password: 'Mot de passe',
      confirmPassword: 'Confirmer le mot de passe',
      continue: 'Continuer',
      authGoogle: 'Connectez-vous avec Google',
    },
  },
  or: 'OU',
  errors: {
    require: 'Ce champs est obligatoire',
    email: 'L\'email est incorrecte',
    passwordLowerCaseCharacter: 'Le mot de passe doit contenir au moins 1 caractère alphabétique minuscule',
    passwordUpperCaseCharacter: 'Le mot de passe doit contenir au moins 1 caractère alphabétique majuscule',
    passwordNumber: 'Le mot de passe doit contenir au moins 1 caractère numérique',
    passwordSpecialCharacter: 'Le mot de passe doit contenir au moins un caractère spécial',
    passwordLength: 'Le mot de passe doit comporter huit caractères ou plus',
    confirmPassword: 'Les mots de passe ne sont pas identique',
  },
};
