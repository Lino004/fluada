import { createNamespacedHelpers } from 'vuex';
import UserAuth from '../../models/auth/UserAuth';

const Auth = createNamespacedHelpers('auth');

export default {
  data() {
    return {
      user: new UserAuth(),
    };
  },
  methods: {
    ...Auth.mapMutations({
      setUser: 'SET_USER',
    }),
    async actionGoogleSignIn() {
      try {
        const user = await this.$gAuth.signIn();
        await this.$store.dispatch('auth/loginGoogle', {
          googleEmail: user.getBasicProfile().getEmail(),
          googleId: user.getBasicProfile().getId(),
        });
        this.$router.push('/');
      } catch (e) {
        this.fdActionErrors(e);
      }
    },
    async actionGoogleSignUp() {
      try {
        const user = await this.$gAuth.signIn();
        await this.$store.dispatch('auth/registerGoogle', {
          googleEmail: user.getBasicProfile().getEmail(),
          googleId: user.getBasicProfile().getId(),
          givenName: user.getBasicProfile().getGivenName(),
          familyName: user.getBasicProfile().getFamilyName(),
        });
        this.$router.push('/');
      } catch (e) {
        this.fdActionErrors(e);
      }
    },
    async actionSignIn() {
      const payload = {
        email: this.user.email,
        password: this.user.password,
      };
      const loader = this.$loading.show();
      try {
        await this.$store.dispatch('auth/login', payload);
        this.$router.push('/');
        loader.hide();
      } catch (e) {
        this.fdActionErrors(e);
        loader.hide();
      }
    },
    async actionSignUp() {
      if (this.user.checkValidation()) return;
      const loader = this.$loading.show();
      try {
        await this.$store.dispatch('auth/register', this.user.payloadRegister());
        if (this.$route.query?.redirect) this.$router.push(this.$route.query?.redirect);
        else this.$router.push('/');
        loader.hide();
      } catch (e) {
        this.fdActionErrors(e);
        loader.hide();
      }
    },
  },
};
