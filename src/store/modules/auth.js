import {
  login, register, logout,
  googleLogin, googleRegister,
} from '../../api/auth';

function initialState() {
  return {
    user: null,
  };
}

const state = initialState();

const namespaced = true;

const mutations = {
  SET_USER: (state, value) => {
    state.user = value;
  },
};

const actions = {
  async login({ commit }, payload) {
    const res = (await login(payload)).data;
    commit('SET_USER', res);
  },
  async loginGoogle({ commit }, payload) {
    const res = (await googleLogin(payload)).data;
    commit('SET_USER', res);
  },
  async registerGoogle({ commit }, payload) {
    const res = (await googleRegister(payload)).data;
    commit('SET_USER', res);
  },
  async register({ commit }, payload) {
    const res = (await register(payload)).data;
    commit('SET_USER', res);
  },
  async logout({ commit }) {
    await logout();
    commit('SET_USER', null);
  },
};

const getters = {
  isLoggedIn: (state) => {
    return !!(state.user);
  },
};

export default {
  namespaced,
  state,
  mutations,
  actions,
  getters,
};
