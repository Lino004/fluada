import { createApp } from 'vue';
import gAuth from 'vue3-google-auth';
import { SnackbarService, Vue3Snackbar } from 'vue3-snackbar';
import VueLoading from 'vue-loading-overlay';
import App from './App.vue';
import i18n from './i18n';
import router from './router';
import store from './store';
import communComponent from './components/common/common';
import common from './mixins/common';

import 'vue-loading-overlay/dist/vue-loading.css';
import 'vue3-snackbar/dist/style.css';
import './assets/css/general.scss';

const app = createApp(App);

communComponent(app);

const $gAuth = gAuth.createGAuth({
  clientId: process.env.VUE_APP_GOOGLE_CLIENT_ID,
});

app.use(store);
app.use(router);
app.use(i18n);
app.use($gAuth);
app.use(SnackbarService);
app.use(VueLoading, {
  color: '#0ea5e9',
});

app.component('vue3-snackbar', Vue3Snackbar);

app.mixin(common);

app.mount('#app');

export default app;
