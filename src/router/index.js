import { createRouter, createWebHistory } from 'vue-router';
import store from '../store';

const routes = [
  {
    path: '/:pathMatch(.*)*',
    name: 'Error',
    component: () => import('../views/Error.vue'),
  },
  {
    path: '/',
    name: 'App',
    component: () => import('../views/app/App.vue'),
    meta: { requiresAuth: true },
    children: [
      {
        path: '',
        name: 'Dashboard',
        component: () => import('../views/app/Dashboard.vue'),
      },
    ],
  },
  {
    path: '/auth',
    name: 'auth',
    redirect: '/auth/sign-in',
    component: () => import('../views/auth/Auth.vue'),
    children: [
      {
        path: 'sign-in',
        name: 'SignIn',
        component: () => import('../views/auth/SignIn.vue'),
      },
      {
        path: 'sign-up',
        name: 'SignUp',
        component: () => import('../views/auth/SignUp.vue'),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth && !store.getters['auth/isLoggedIn']) {
    next({
      name: 'auth',
      query: { redirect: to.fullPath },
    });
  } else next();
});

export default router;
