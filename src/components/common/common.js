import FbCard from './FdCard.vue';
import FbInput from './FdInput.vue';
import FbButton from './FdButton.vue';

export default function components(app) {
  app.component('fd-card', FbCard);
  app.component('fd-input', FbInput);
  app.component('fd-button', FbButton);
}
