import isEmail from 'validator/lib/isEmail';
import isEmpty from 'validator/lib/isEmpty';
import User from './User';

export default class UserAuth extends User {
  constructor() {
    super();

    this.password = 'B@ndito96*';
    this.confirmPassword = 'B@ndito96*';

    this.initErrors();
  }

  initErrors() {
    this.fistNameErrors = [];
    this.lastNameErrors = [];
    this.emailErrors = [];
    this.passwordErrors = [];
    this.confirmPasswordErrors = [];
  }

  checkValidation() {
    this.initErrors();
    if (isEmpty(this.fistName)) {
      this.fistNameErrors.push('auth.errors.require');
    }
    if (isEmpty(this.lastName)) {
      this.lastNameErrors.push('auth.errors.require');
    }
    if (!isEmail(this.email)) {
      this.emailErrors.push('auth.errors.email');
    }
    if (!(/(?=.*[a-z])/.test(this.password))) {
      this.passwordErrors.push('auth.errors.passwordLowerCaseCharacter');
    }
    if (!(/(?=.*[A-Z])/.test(this.password))) {
      this.passwordErrors.push('auth.errors.passwordUpperCaseCharacter');
    }
    if (!(/(?=.*[0-9])/.test(this.password))) {
      this.passwordErrors.push('auth.errors.passwordNumber');
    }
    if (!(/(?=.*[!@#$%^&*])/.test(this.password))) {
      this.passwordErrors.push('auth.errors.passwordSpecialCharacter');
    }
    if (!(/(?=.{10,})/.test(this.password))) {
      this.passwordErrors.push('auth.errors.passwordLength');
    }
    if (this.password !== this.confirmPassword) {
      this.confirmPasswordErrors.push('auth.errors.confirmPassword');
    }
    return !!(this.fistNameErrors.length)
      || !!(this.lastNameErrors.length)
      || !!(this.emailErrors.length)
      || !!(this.passwordErrors.length)
      || !!(this.confirmPasswordErrors.length);
  }

  payloadRegister() {
    return {
      email: this.email,
      givenName: this.fistName,
      familyName: this.lastName,
      password: this.password,
    };
  }
}
